package com.heendher.codepolitannewsreadermobileapp.Template;

/**
 * Created by -H- on 9/8/2015.
 */
public interface EndpointAPI {
    String LATEST_POST = "http://www.codepolitan.com/api/posts/latest/post/";
    String SEARCH = "http://www.codepolitan.com/api/posts/search";
    String KATEGORI = "http://www.codepolitan.com/api/posts/by_category/";
    String POST_TYPE = "http://www.codepolitan.com/api/posts/latest/";
    String DETAIL = "http://www.codepolitan.com/api/posts/detail/";
    String TAGS = "http://www.codepolitan.com/api/tags/popular/";
    String POST_TAG = "http://www.codepolitan.com/api/posts/by_tag/";
}
