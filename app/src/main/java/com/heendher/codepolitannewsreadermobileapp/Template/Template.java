package com.heendher.codepolitannewsreadermobileapp.Template;

/**
 * Created by -H- on 9/8/2015.
 */
public interface Template {
    interface Global {
        String ARRAY = "result";
        String TAGS = "tags";
        String CONTENT = "content";
        String CODE = "code";
        String CODESUCCESS = "200";
        String CODEERROR = "400";
    }

    interface Post {
        String ID = "id";
        String SLUG = "slug";
        String TITLE = "title";
        String EXCERPT = "excerpt";
        String DATE = "date";
        String DATE_CLEAR = "date_clear";
        String LINK = "link";
        String THUMBNAIL_SMALL = "thumbnail_small";
        String THUMBNAIL_MEDIUM = "thumbnail_medium";
    }

    interface Detail {
        String ID = "id";
        String TITLE = "title";
        String CONTENT = "content";
        String DATE = "date";
        String DATE_CLEAR = "date_clear";
        String LINK = "link";
        String THUMBNAIL_SMALL = "thumbnail_small";
        String THUMBNAIL_MEDIUM = "thumbnail_medium";
        String THUMBNAIL_LARGE = "thumbnail_large";
        String ARRAY = "result";
        String TAGS = "tags";
        String NAME = "name";
        String SLUG = "slug";
    }

    interface Activity{
        String MAIN = "main";
        String SEARCH_RESULT = "search_result";
        String DETAIL = "detail";
        String TAGS_ACTIVITY = "tags_activity";
    }


    interface Drawer {
        String HOME = "Home";
        String TAGS = "Tags";
        String NYANKOMIK = "Nyankomik";
        String MEME = "Meme";
        String QUOTES = "Quotes";
        String KARYALOKAL = "KaryaLokal";
        String NYANKODMAGZ = "nyankodMagz";
        String PUSAKACMS = "PusakaCMS";
        String TENTANG = "Tentang";
        String PARTNERS = "Partners";
        String CONTACTUS = "Contact Us";
    }

    interface PostType{
        String MEME = "meme";
        String NYANKOMIK = "nyankomik";
        String QUOTES = "quotes";
    }

    interface KategoriName {
        String ALLPOSTS = "All Posts";
        String COMIC = "Comic";
        String EVENT = "Event";
        String INFO = "Info";
        String KOMUNITAS = "Komunitas";
        String NEWS = "News";
        String OPINI = "Opini";
        String KARYALOKAL = "Review KaryaLokal";
        String TIPS = "Tips & Trik";
        String TOKOH = "Tokoh";
        String TOOLS = "Tools";
        String WAWANCARA = "Wawancara";
    }

    interface KategoriSlug {
        String ALLPOSTS = "All Posts";
        String COMIC = "comic";
        String EVENT = "event-2";
        String INFO = "info";
        String KOMUNITAS = "komunitas-2";
        String NEWS = "news";
        String OPINI = "opini";
        String KARYALOKAL = "review-karyalokal";
        String TIPS = "tips-trik";
        String TOKOH = "tokoh";
        String TOOLS = "tools";
        String WAWANCARA = "wawancara";
    }

    interface Tags{
        String NAME = "name";
        String SLUG = "slug";
        String COUNT = "count";
        String AZ = "A-Z";
        String ZA = "Z-A";
        String POPULAR = "Popular";

    }

    interface ConnectionProblem{
        String INTERNETERROR = "internet";
        String SERVERERROR = "server";
        String MSGINTERNET = "Oops.. Please check your internet connection";
        String MSGSERVER = "Sorry.. Our server temporarily down";
    }

    interface VolleyRetryPolicy{
        int SOCKET_TIMEOUT = 3000;
        int RETRIES = 1;
    }

}
