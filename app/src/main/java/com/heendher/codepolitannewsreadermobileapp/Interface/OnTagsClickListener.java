package com.heendher.codepolitannewsreadermobileapp.Interface;

import com.heendher.codepolitannewsreadermobileapp.POJO.TagsModel;

/**
 * Created by -H- on 9/13/2015.
 */
public interface OnTagsClickListener {
    void onClickListener(TagsModel tag);
}
