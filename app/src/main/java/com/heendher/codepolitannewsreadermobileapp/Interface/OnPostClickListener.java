package com.heendher.codepolitannewsreadermobileapp.Interface;

import com.heendher.codepolitannewsreadermobileapp.POJO.PostModel;

/**
 * Created by -H- on 9/10/2015.
 */
public interface OnPostClickListener {
    void OnClickListener(PostModel post);
    void OnShareClickListener(String subject, String content);
}
