package com.heendher.codepolitannewsreadermobileapp.Activity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.heendher.codepolitannewsreadermobileapp.Fragment.DetailFragment;
import com.heendher.codepolitannewsreadermobileapp.Fragment.ErrorFragment;
import com.heendher.codepolitannewsreadermobileapp.R;
import com.heendher.codepolitannewsreadermobileapp.Template.Template;


public class DetailActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private Drawable mActionBarBackgroundDrawable;
    private int mId;
    private String mLastAccess, mTitle, mContent, mExcerpt, mLink, mDate, mImage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mActionBarBackgroundDrawable = mToolbar.getBackground();
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        mId = getIntent().getExtras().getInt("id");
        mTitle = getIntent().getExtras().getString("title");
        mImage = getIntent().getExtras().getString("image_id");
        mExcerpt = getIntent().getExtras().getString("excerpt");
        mLink = getIntent().getExtras().getString("link");
        mDate = getIntent().getExtras().getString("date");
        setFragmentLayout(Template.Activity.DETAIL, "");


    }

    public void setFragmentLayout(String tag, String error) {

        Bundle bundle = new Bundle();
        bundle.putInt("id", mId);
        bundle.putString("title", mTitle);
        bundle.putString("date", mDate);
        bundle.putString("image_id", mImage);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment fragment;
        if (tag.equals("error")) {
            fragment = new ErrorFragment();
            bundle.putString("error", error);

            bundle.putString("last_access", mLastAccess);
            bundle.putString("get_activity", Template.Activity.DETAIL);
            fragment.setArguments(bundle);
        } else {
            fragment = new DetailFragment();
            mLastAccess = tag;
            fragment.setArguments(bundle);
        }

        fragmentTransaction.replace(R.id.fragment_container, fragment);
        fragmentTransaction.commit();
        if (tag.equals("error"))
            setTitle("Oops");
        else
            setTitle(tag);
    }

    void setTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    public void shareIt(String subject, String content) {
        Intent intent = new Intent(android.content.Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, content);
        startActivity(Intent.createChooser(intent, "Share via"));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.shareMenu) {
            shareIt(mTitle, mContent);
        } else if (id == android.R.id.home)
            finish();

        return super.onOptionsItemSelected(item);
    }


}
