package com.heendher.codepolitannewsreadermobileapp.Activity;

import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import com.heendher.codepolitannewsreadermobileapp.Fragment.ContactFragment;
import com.heendher.codepolitannewsreadermobileapp.Fragment.DrawerFragment;
import com.heendher.codepolitannewsreadermobileapp.Fragment.ErrorFragment;
import com.heendher.codepolitannewsreadermobileapp.Fragment.MemeFragment;
import com.heendher.codepolitannewsreadermobileapp.Fragment.NyankomikFragment;
import com.heendher.codepolitannewsreadermobileapp.Fragment.OurNetworkFragment;
import com.heendher.codepolitannewsreadermobileapp.Fragment.OurPartnersFragment;
import com.heendher.codepolitannewsreadermobileapp.Fragment.TagsFragment;
import com.heendher.codepolitannewsreadermobileapp.Fragment.PostFragment;
import com.heendher.codepolitannewsreadermobileapp.Fragment.QuotesFragment;
import com.heendher.codepolitannewsreadermobileapp.Fragment.TentangFragment;
import com.heendher.codepolitannewsreadermobileapp.Interface.OnSpinnerClickListener;
import com.heendher.codepolitannewsreadermobileapp.POJO.SpinnerModel;
import com.heendher.codepolitannewsreadermobileapp.R;
import com.heendher.codepolitannewsreadermobileapp.Template.Template;

import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends AppCompatActivity {

    private boolean mInSearchMode = false;
    private Toolbar mToolbar;
    private DrawerFragment mDrawerFragment;
    private SearchView mSearchView;
    private OnSpinnerClickListener mOnSpinnerClick;
    private boolean mReadyToClose = false;
    private boolean mIsDrawerOpen = true;
    private View mSpinnerContainer;
    private Spinner mSpinner;
    private ActionBar.LayoutParams mLp;
    private static String sLAST_ACCESS;
    private boolean mMenuIsReady = false;
    private MenuItem mMenuItem;
    private String mTag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);


        mSpinnerContainer = LayoutInflater.from(this).inflate(R.layout.toolbar_spinner,
                mToolbar, false);
        mLp = new ActionBar.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mSpinner = (Spinner) mSpinnerContainer.findViewById(R.id.toolbar_spinner);

        setFragmentLayout(Template.Drawer.HOME, "");

        mDrawerFragment = (DrawerFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_drawer);
        mDrawerFragment.setUp((DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);

    }

    void addSpinner() {
        com.heendher.codepolitannewsreadermobileapp.Adapter.SpinnerAdapter spinnerAdapter =
                new com.heendher.codepolitannewsreadermobileapp.Adapter.SpinnerAdapter(getSpinnerData(), getApplicationContext());

        mSpinner.setAdapter(spinnerAdapter);
        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                mOnSpinnerClick.OnItemClickListener(kategoriToQuery(((SpinnerModel) adapterView.getItemAtPosition(i)).getTitle()));

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        mToolbar.addView(mSpinnerContainer, mLp);
    }

    void removeSpinner() {
        mToolbar.removeView(mSpinnerContainer);
        mSpinner.setAdapter(null);
    }

    List<SpinnerModel> getSpinnerData() {
        List<SpinnerModel> list = new ArrayList<>();
        list.add(new SpinnerModel(Template.KategoriName.ALLPOSTS));
        list.add(new SpinnerModel(Template.KategoriName.COMIC));
        list.add(new SpinnerModel(Template.KategoriName.EVENT));
        list.add(new SpinnerModel(Template.KategoriName.INFO));
        list.add(new SpinnerModel(Template.KategoriName.KOMUNITAS));
        list.add(new SpinnerModel(Template.KategoriName.NEWS));
        list.add(new SpinnerModel(Template.KategoriName.OPINI));
        list.add(new SpinnerModel(Template.KategoriName.KARYALOKAL));
        list.add(new SpinnerModel(Template.KategoriName.TIPS));
        list.add(new SpinnerModel(Template.KategoriName.TOKOH));
        list.add(new SpinnerModel(Template.KategoriName.TOOLS));
        list.add(new SpinnerModel(Template.KategoriName.WAWANCARA));
        return list;
    }

    List<SpinnerModel> getSpinnerDataTags() {
        List<SpinnerModel> list = new ArrayList<>();
        list.add(new SpinnerModel(Template.Tags.POPULAR));
        list.add(new SpinnerModel(Template.Tags.AZ));
        list.add(new SpinnerModel(Template.Tags.ZA));

        return list;
    }

    String kategoriToQuery(String title) {
        if (title.equals(Template.KategoriName.ALLPOSTS))
            return Template.KategoriSlug.ALLPOSTS;
        else if (title.equals(Template.KategoriName.COMIC))
            return Template.KategoriSlug.COMIC;
        else if (title.equals(Template.KategoriName.EVENT))
            return Template.KategoriSlug.EVENT;
        else if (title.equals(Template.KategoriName.INFO))
            return Template.KategoriSlug.INFO;
        else if (title.equals(Template.KategoriName.KARYALOKAL))
            return Template.KategoriSlug.KARYALOKAL;
        else if (title.equals(Template.KategoriName.KOMUNITAS))
            return Template.KategoriSlug.KOMUNITAS;
        else if (title.equals(Template.KategoriName.NEWS))
            return Template.KategoriSlug.NEWS;
        else if (title.equals(Template.KategoriName.OPINI))
            return Template.KategoriSlug.OPINI;
        else if (title.equals(Template.KategoriName.TIPS))
            return Template.KategoriSlug.TIPS;
        else if (title.equals(Template.KategoriName.TOKOH))
            return Template.KategoriSlug.TOKOH;
        else if (title.equals(Template.KategoriName.TOOLS))
            return Template.KategoriSlug.TOOLS;
        else
            return Template.KategoriSlug.WAWANCARA;
    }

    public void setFragmentLayout(String tag, String errorMessage) {
        Bundle bundle = new Bundle();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment fragment = new Fragment();
        mTag = tag;
        removeSpinner();
        if (!tag.equals("error"))
            sLAST_ACCESS = tag;

        if (tag.equals(Template.Drawer.TAGS)) {
            fragment = new TagsFragment();
        } else if (tag.equals(Template.Drawer.HOME)) {
            fragment = new PostFragment();
            addSpinner();

        } else if (tag.equals("error")) {

            bundle.putString("error", errorMessage);
            bundle.putString("last_access", sLAST_ACCESS);
            bundle.putString("get_activity", Template.Activity.MAIN);
            fragment = new ErrorFragment();

        } else if (tag.equals(Template.Drawer.NYANKOMIK)) {
            fragment = new NyankomikFragment();
        } else if (tag.equals(Template.Drawer.MEME)) {
            fragment = new MemeFragment();
        } else if (tag.equals(Template.Drawer.QUOTES)) {
            fragment = new QuotesFragment();
        } else if (tag.equals(Template.Drawer.KARYALOKAL) || tag.equals(Template.Drawer.NYANKODMAGZ) || tag.equals(Template.Drawer.PUSAKACMS)) {
            fragment = new OurNetworkFragment();
            bundle.putString("our_network", tag);
        } else if (tag.equals(Template.Drawer.PARTNERS)) {
            fragment = new OurPartnersFragment();
        } else if (tag.equals(Template.Drawer.CONTACTUS)) {
            fragment = new ContactFragment();
        } else if (tag.equals(Template.Drawer.TENTANG)) {
            fragment = new TentangFragment();
        }

        fragment.setArguments(bundle);
        fragmentTransaction.replace(R.id.framelayout_home, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
        setTitle(tag);
    }

    void setTitle(String title) {
        if (title.equals("error"))
            getSupportActionBar().setTitle("Oops");
        else
            getSupportActionBar().setTitle(title);
    }

    public void setSpinnerClickListener(OnSpinnerClickListener onSpinnerClickListener) {
        mOnSpinnerClick = onSpinnerClickListener;
    }

    public void shareIt(String subject, String content) {
        Intent intent = new Intent(android.content.Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, content);
        startActivity(Intent.createChooser(intent, "Share via"));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_home_search, menu);

        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        mSearchView =
                (SearchView) menu.findItem(R.id.search).getActionView();
        mSearchView.setSearchableInfo(
                searchManager.getSearchableInfo(new ComponentName(this, SearchResultActivity.class)));
        //mSearchView.setQueryHint(getResources().getString(R.string.search_hint));


        mSearchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                searchMode(true);

            }
        });

        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        searchMode(false);
                    }
                }, 1000);


                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        mSearchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {

                searchMode(false);
                return false;
            }
        });


        return true;
    }


    void searchMode(Boolean isEnabled) {
        if (isEnabled) {
            mInSearchMode = true;
            mDrawerFragment.setDrawerIndicator(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    searchMode(false);
                }
            });

        } else {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            mToolbar.setNavigationOnClickListener(null);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            mSearchView.onActionViewCollapsed();

            mInSearchMode = false;
            mDrawerFragment.setDrawerIndicator(true);


        }


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        if (id == android.R.id.home) {

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        mIsDrawerOpen = mDrawerFragment.getDrawerStatus();
        if (mInSearchMode) {
            searchMode(false);
        } else if (mIsDrawerOpen) {
            mDrawerFragment.closeDrawer();
        } else {
            if (mReadyToClose)
                super.onBackPressed();
            else {
                mReadyToClose = true;
                Toast.makeText(getApplicationContext(), "Press back again to exit", Toast.LENGTH_SHORT).show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mReadyToClose = false;
                    }
                }, 2000);
            }
        }

    }
}
