package com.heendher.codepolitannewsreadermobileapp.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;


import com.heendher.codepolitannewsreadermobileapp.Fragment.ErrorFragment;
import com.heendher.codepolitannewsreadermobileapp.Fragment.TagsResultFragment;
import com.heendher.codepolitannewsreadermobileapp.R;
import com.heendher.codepolitannewsreadermobileapp.Template.Template;

public class TagsActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private String mLastAccess;
    private String mSlug, mType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mSlug = getIntent().getExtras().getString("slug");
        mType = getIntent().getExtras().getString("type");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        setFragmentLayout(Template.Activity.TAGS_ACTIVITY, "");
    }

    public void setFragmentLayout(String tag, String error) {

        Bundle bundle = new Bundle();
        bundle.putString("slug", mSlug);
        bundle.putString("type", mType);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment fragment;
        if (tag.equals("error")) {
            fragment = new ErrorFragment();
            bundle.putString("last_access", mLastAccess);
            if (error.equals(Template.ConnectionProblem.INTERNETERROR))
                bundle.putString("error", Template.ConnectionProblem.MSGINTERNET);
            else
                bundle.putString("error", Template.ConnectionProblem.MSGSERVER);
            bundle.putString("get_activity", Template.Activity.TAGS_ACTIVITY);
            fragment.setArguments(bundle);
        } else {
            fragment = new TagsResultFragment();
            mLastAccess = tag;
            fragment.setArguments(bundle);
        }

        fragmentTransaction.replace(R.id.fragment_container, fragment);
        fragmentTransaction.commit();
        if (tag.equals("error"))
            setTitle("Oops");
        else
            setTitle(mSlug);
    }

    void setTitle(String title) {
        getSupportActionBar().setTitle(title);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        /*MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search_result, menu);

        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        mSearchView =
                (SearchView) menu.findItem(R.id.search).getActionView();
        mSearchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));
        mSearchView.setQueryHint(getResources().getString(R.string.search_hint));


        mSearchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


            }
        });

        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {


                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        mSearchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {

                return false;
            }
        });
*/
        return true;
    }

    public void shareIt(String subject, String content) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, content);
        startActivity(Intent.createChooser(intent, "Share via"));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
    }


}
