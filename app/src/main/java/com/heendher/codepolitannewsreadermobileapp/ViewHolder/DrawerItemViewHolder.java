package com.heendher.codepolitannewsreadermobileapp.ViewHolder;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.heendher.codepolitannewsreadermobileapp.Adapter.DrawerRecyclerAdapter;
import com.heendher.codepolitannewsreadermobileapp.POJO.DrawerModel;
import com.heendher.codepolitannewsreadermobileapp.R;

import java.util.List;


/**
 * Created by -H- on 9/9/2015.
 */
public class DrawerItemViewHolder extends MainViewHolder implements  View.OnClickListener {

    public TextView mTextView;
    public ImageView mImageView;
    public RelativeLayout mBackgroundRelative;
    private DrawerRecyclerAdapter.OnClickListener mOnClick;
    private List<DrawerModel> mPostList;
    public DrawerItemViewHolder(View itemView, DrawerRecyclerAdapter.OnClickListener onClickListener, List<DrawerModel> postList) {
        super(itemView);
        mTextView = (TextView)itemView.findViewById(R.id.drawer_item_text);
        mImageView = (ImageView)itemView.findViewById(R.id.drawer_item_image);
        mBackgroundRelative = (RelativeLayout)itemView.findViewById(R.id.drawerItemBackground);
        mPostList = postList;
        itemView.setOnClickListener(this);
        mOnClick = onClickListener;
    }


    @Override
    public void onClick(View view) {
        mOnClick.OnItemClickListener(mPostList.get(getLayoutPosition()).getTitle(),getLayoutPosition());

    }
}
