package com.heendher.codepolitannewsreadermobileapp.ViewHolder;

import android.view.View;
import android.widget.TextView;

import com.heendher.codepolitannewsreadermobileapp.Adapter.DrawerRecyclerAdapter;
import com.heendher.codepolitannewsreadermobileapp.R;

/**
 * Created by -H- on 9/9/2015.
 */
public class DrawerSubViewHolder extends MainViewHolder{

    public TextView mTextView;

    public DrawerSubViewHolder(View itemView, DrawerRecyclerAdapter.OnClickListener onClickListener) {
        super(itemView);
        mTextView = (TextView)itemView.findViewById(R.id.drawer_item_text_sub);

    }



}
