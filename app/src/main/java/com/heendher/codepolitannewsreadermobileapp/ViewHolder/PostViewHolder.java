package com.heendher.codepolitannewsreadermobileapp.ViewHolder;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.heendher.codepolitannewsreadermobileapp.Interface.OnPostClickListener;
import com.heendher.codepolitannewsreadermobileapp.POJO.PostModel;
import com.heendher.codepolitannewsreadermobileapp.R;

import java.util.List;

/**
 * Created by -H- on 9/8/2015.
 */
public class PostViewHolder extends MainViewHolder implements View.OnClickListener{

    public TextView mTitle,mExcerpt,mDate;
    public ImageView mShare,mThumbnail;
    private OnPostClickListener mOnItem;
    private List mPostList;
    public LinearLayout mPostLayout;
    public PostViewHolder(View itemView, OnPostClickListener onItemClickListener, List postList) {
        super(itemView);
        mPostList = postList;
        mOnItem = onItemClickListener;
        mTitle = (TextView)itemView.findViewById(R.id.post_title);
        mExcerpt = (TextView)itemView.findViewById(R.id.post_excerpt);
        mDate = (TextView)itemView.findViewById(R.id.post_date);
        mShare = (ImageView)itemView.findViewById(R.id.post_share);
        mThumbnail = (ImageView)itemView.findViewById(R.id.post_thumbnail);
        mPostLayout = (LinearLayout) itemView.findViewById(R.id.post_layout);
        mPostLayout.setOnClickListener(this);
        mTitle.setOnClickListener(this);
        mExcerpt.setOnClickListener(this);
        mDate.setOnClickListener(this);
        mShare.setOnClickListener(this);
        mThumbnail.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if(view.getId()==R.id.post_share){
            mOnItem.OnShareClickListener(((PostModel)mPostList.get(getLayoutPosition())).getTitle(),((PostModel)mPostList.get(getLayoutPosition())).getLink());
        }else
            mOnItem.OnClickListener((PostModel)mPostList.get(getLayoutPosition()));
    }
}
