package com.heendher.codepolitannewsreadermobileapp.ViewHolder;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.heendher.codepolitannewsreadermobileapp.Interface.OnTagsClickListener;
import com.heendher.codepolitannewsreadermobileapp.POJO.TagsModel;
import com.heendher.codepolitannewsreadermobileapp.R;

import java.util.List;

/**
 * Created by -H- on 9/13/2015.
 */
public class TagsViewHolder extends MainViewHolder implements View.OnClickListener{

    public LinearLayout mLayout;
    public OnTagsClickListener mOnItemClick;
    public List mList;
    public TextView mName,mCount;
    public ProgressBar mCountProgress;
    public TagsViewHolder(View itemView, OnTagsClickListener onItemClickListener, List list) {
        super(itemView);
        mLayout = (LinearLayout)itemView.findViewById(R.id.tags_layout);
        mLayout.setOnClickListener(this);
        mName = (TextView)itemView.findViewById(R.id.tag_name);
        mCount = (TextView)itemView.findViewById(R.id.tag_size);
        mCountProgress = (ProgressBar)itemView.findViewById(R.id.tag_progress);
        mOnItemClick = onItemClickListener;
        mList = list;
    }

    @Override
    public void onClick(View view) {
        mOnItemClick.onClickListener(((TagsModel)mList.get(getLayoutPosition())));
    }
}
