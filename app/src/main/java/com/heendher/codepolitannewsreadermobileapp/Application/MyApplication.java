package com.heendher.codepolitannewsreadermobileapp.Application;

import android.app.Application;
import android.content.Context;

/**
 * Created by -H- on 9/7/2015.
 */
public class MyApplication extends Application {
    private static MyApplication myApplication;

    @Override
    public void onCreate() {
        super.onCreate();
        myApplication = this;
    }

    public static MyApplication getInstance(){
        return myApplication;
    }

    public static Context getContext(){
        return myApplication.getApplicationContext();
    }
}
