package com.heendher.codepolitannewsreadermobileapp.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.heendher.codepolitannewsreadermobileapp.POJO.DrawerModel;
import com.heendher.codepolitannewsreadermobileapp.R;
import com.heendher.codepolitannewsreadermobileapp.ViewHolder.DividerViewHolder;
import com.heendher.codepolitannewsreadermobileapp.ViewHolder.DrawerItemViewHolder;
import com.heendher.codepolitannewsreadermobileapp.ViewHolder.DrawerSubViewHolder;

import java.util.Collections;
import java.util.List;

/**
 * Created by -H- on 8/30/2015.
 */
public class DrawerRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private static final int sTYPE_SUB = 0;
    private static final int sTYPE_ITEM = 1;
    private static final int sDIVIDER = 2;
    private static final int sHEADER = 3;
    private LayoutInflater mInflater;
    private List<DrawerModel> mItems = Collections.emptyList();
    private Context mContext;
    private OnClickListener mOnClickListener;
    public static int SELECTED_ITEM = 1;

    public DrawerRecyclerAdapter(Context context, List<DrawerModel> items, OnClickListener onClickListener) {
        mOnClickListener = onClickListener;
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mItems = items;

    }

    @Override
    public int getItemViewType(int position) {
        int viewType;
        if(position==0)
            viewType = sHEADER;
        else if (mItems.get(position).isItem())
            viewType = sTYPE_ITEM;
        else if (mItems.get(position).isTitle())
            viewType = sTYPE_SUB;
        else
            viewType = sDIVIDER;
        return viewType;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view;
        switch (i){
            case sTYPE_ITEM:
                view = mInflater.inflate(R.layout.row_drawer_item, viewGroup, false);
                DrawerItemViewHolder drawerItem = new DrawerItemViewHolder(view, mOnClickListener, mItems);
                return drawerItem;
            case sTYPE_SUB:
                view = mInflater.inflate(R.layout.row_drawer_sub,viewGroup,false);
                DrawerSubViewHolder drawerSub = new DrawerSubViewHolder(view,mOnClickListener);
                return drawerSub;
            case sDIVIDER:
                view = mInflater.inflate(R.layout.divider,viewGroup,false);
                DividerViewHolder divider = new DividerViewHolder(view, mOnClickListener);
                return divider;
            case sHEADER:
                view = mInflater.inflate(R.layout.row_drawer_header,viewGroup,false);
                DividerViewHolder dividerViewHolder = new DividerViewHolder(view,mOnClickListener);
                return dividerViewHolder;
            default:
                view = mInflater.inflate(R.layout.row_drawer_item, viewGroup, false);
                DrawerItemViewHolder drawerItemDefault = new DrawerItemViewHolder(view,mOnClickListener, mItems);
                return drawerItemDefault;
        }

    }

    void delete(int position) {
        mItems.remove(position);
        notifyItemRemoved(position);
    }



    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        switch (viewHolder.getItemViewType()){
            case sTYPE_ITEM:
                DrawerItemViewHolder drawerItem = (DrawerItemViewHolder)viewHolder;
                drawerItem.mTextView.setText(mItems.get(i).getTitle());
                if(i==SELECTED_ITEM){
                    drawerItem.mImageView.setImageResource(mItems.get(i).getIdSelected());
                    drawerItem.mImageView.setAlpha(1f);
                    drawerItem.mTextView.setTextColor(mContext.getResources().getColor(R.color.primaryColor));
                    drawerItem.mBackgroundRelative.setBackgroundColor(mContext.getResources().getColor(R.color.drawer_selected));
                }else{
                    drawerItem.mImageView.setImageResource(mItems.get(i).getId());
                    drawerItem.mImageView.setAlpha(0.54f);
                    drawerItem.mTextView.setTextColor(mContext.getResources().getColor(android.R.color.white));
                    drawerItem.mBackgroundRelative.setBackgroundColor(mContext.getResources().getColor(R.color.activity_background));
                }


                break;
            case sTYPE_SUB:
                DrawerSubViewHolder drawerSub = (DrawerSubViewHolder)viewHolder;
                drawerSub.mTextView.setText(mItems.get(i).getTitle());
                break;
            case sDIVIDER:
                DividerViewHolder divider = (DividerViewHolder)viewHolder;
                break;

        }

    }


    @Override
    public int getItemCount() {
        return mItems.size();
    }


    public interface OnClickListener{
        void OnItemClickListener(String title, int position);
    }


}
