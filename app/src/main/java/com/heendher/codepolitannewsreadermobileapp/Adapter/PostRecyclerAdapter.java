package com.heendher.codepolitannewsreadermobileapp.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.heendher.codepolitannewsreadermobileapp.Interface.OnPostClickListener;
import com.heendher.codepolitannewsreadermobileapp.POJO.PostModel;
import com.heendher.codepolitannewsreadermobileapp.R;
import com.heendher.codepolitannewsreadermobileapp.Utils.AnimationUtil;
import com.heendher.codepolitannewsreadermobileapp.Utils.TimeRelative;
import com.heendher.codepolitannewsreadermobileapp.Utils.VolleySingleton;
import com.heendher.codepolitannewsreadermobileapp.ViewHolder.PostViewHolder;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.List;

/**
 * Created by -H- on 9/8/2015.
 */
public class PostRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    public static final int TYPE_HOME = 0;
    public static final int TYPE_MEME = 1;
    public static final int TYPE_KOMIK = 2;
    public static final int TYPE_QUOTES = 3;
    private static final int sTYPE_HEADER = 0;
    private static final int sTYPE_ITEMLEFT = 1;
    private static final int sTYPE_ITEMRIGHT = 2;
    private int mAdapterType;
    private TimeRelative mTimeRelative;
    private List<PostModel> postList = Collections.emptyList();
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private ImageLoader mImageLoader;
    private OnPostClickListener mOnItemClick;
    private int mLastPosition;

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = mLayoutInflater;
        View viewHeader;
        PostViewHolder postHeader;
        switch (viewType) {
            case TYPE_HOME:
                viewHeader = inflater.inflate(R.layout.row_post_one, parent, false);
                postHeader = new PostViewHolder(viewHeader, mOnItemClick, postList);
                return postHeader;
            case TYPE_MEME:
                viewHeader = inflater.inflate(R.layout.row_post_two, parent, false);
                postHeader = new PostViewHolder(viewHeader, mOnItemClick, postList);
                return postHeader;
            case TYPE_QUOTES:
                viewHeader = inflater.inflate(R.layout.row_post_two, parent, false);
                postHeader = new PostViewHolder(viewHeader, mOnItemClick, postList);
                return postHeader;
            default:
                viewHeader = inflater.inflate(R.layout.row_post_one, parent, false);
                postHeader = new PostViewHolder(viewHeader, mOnItemClick, postList);
                return postHeader;
        }


    }

    @Override
    public int getItemViewType(int position) {
        int viewType;
        if (mAdapterType == TYPE_HOME) {
            viewType = TYPE_HOME;
        } else if (mAdapterType == TYPE_MEME) {
            viewType = TYPE_MEME;
        } else if (mAdapterType == TYPE_QUOTES) {
            viewType = TYPE_QUOTES;
        } else {
            viewType = TYPE_KOMIK;
        }

        return viewType;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {


        final PostViewHolder postHeader = (PostViewHolder) holder;
        //postHeader.mTitle.setText("Le le");
        postHeader.mTitle.setText(postList.get(position).getTitle());
        postHeader.mExcerpt.setText(postList.get(position).getExcerpt());
        //postHeader.mDate.setText("waktu");
        postHeader.mDate.setText(mTimeRelative.getRelativeTime(postList.get(position).getDate(), mContext));
        Picasso.with(mContext).load(postList.get(position).getThumbnailMedium()).into(postHeader.mThumbnail);
        if (mAdapterType == TYPE_HOME) {
            if (position == 0) {

            } else if (position % 2 == 1) {
                RecyclerView.LayoutParams layoutParams = new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT);
                layoutParams.setMargins(16, 16, 8, 0);
                postHeader.mPostLayout.setLayoutParams(layoutParams);
                postHeader.mExcerpt.setVisibility(TextView.GONE);
            } else {
                RecyclerView.LayoutParams layoutParams = new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT);
                layoutParams.setMargins(8, 16, 16, 0);
                postHeader.mPostLayout.setLayoutParams(layoutParams);
                postHeader.mExcerpt.setVisibility(TextView.GONE);
            }

        } else if (mAdapterType == TYPE_KOMIK) {
            if (position % 2 == 0) {
                RecyclerView.LayoutParams layoutParams = new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT);
                layoutParams.setMargins(16, 16, 8, 0);
                postHeader.mPostLayout.setLayoutParams(layoutParams);
                postHeader.mExcerpt.setVisibility(TextView.GONE);
            } else {
                RecyclerView.LayoutParams layoutParams = new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT);
                layoutParams.setMargins(8, 16, 16, 0);
                postHeader.mPostLayout.setLayoutParams(layoutParams);
                postHeader.mExcerpt.setVisibility(TextView.GONE);
            }
        }else if(mAdapterType == TYPE_MEME){
            postHeader.mExcerpt.setVisibility(TextView.GONE);
        }else{
            postHeader.mTitle.setVisibility(TextView.GONE);
            postHeader.mExcerpt.setVisibility(TextView.GONE);
        }

        //mImageLoader.get(postList.get(position).getThumbnailSmall(),ImageLoader.getImageListener(postHeader.mThumbnail,R.drawable.ic_action_search,R.drawable.ic_action_share));

        if (position > mLastPosition) {
            AnimationUtil.animate(holder, true);
        } else {
            AnimationUtil.animate(holder, false);
        }
        mLastPosition = position;
    }


    public PostRecyclerAdapter(Context context, List<PostModel> postList, OnPostClickListener onClickListener, int adapterType) {
        mOnItemClick = onClickListener;
        mImageLoader = VolleySingleton.getInstance().getImageLoader();
        mContext = context;
        mLayoutInflater = LayoutInflater.from(mContext);
        mTimeRelative = new TimeRelative();
        mAdapterType = adapterType;
        this.postList = postList;
    }


    @Override
    public int getItemCount() {
        return postList.size();
    }
}
