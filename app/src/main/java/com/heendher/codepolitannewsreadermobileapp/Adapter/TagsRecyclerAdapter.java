package com.heendher.codepolitannewsreadermobileapp.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.toolbox.ImageLoader;
import com.heendher.codepolitannewsreadermobileapp.Interface.OnTagsClickListener;
import com.heendher.codepolitannewsreadermobileapp.POJO.TagsModel;
import com.heendher.codepolitannewsreadermobileapp.R;
import com.heendher.codepolitannewsreadermobileapp.Utils.TimeRelative;
import com.heendher.codepolitannewsreadermobileapp.Utils.VolleySingleton;
import com.heendher.codepolitannewsreadermobileapp.ViewHolder.TagsViewHolder;

import java.util.Collections;
import java.util.List;

/**
 * Created by -H- on 9/8/2015.
 */
public class TagsRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int sTYPE_ITEMLEFT = 0;
    private static final int sTYPE_ITEMRIGHT = 1;
    private TimeRelative mTimeRelative;
    private List<TagsModel> postList = Collections.emptyList();
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private ImageLoader mImageLoader;
    private OnTagsClickListener onTagsClickListener;
    private static int sCOUNT_MAX;
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = mLayoutInflater;
        View viewHeader = inflater.inflate(R.layout.row_tags, parent, false);
        TagsViewHolder tagsModel = new TagsViewHolder(viewHeader, onTagsClickListener, postList);
        return tagsModel;
    }


    @Override
    public int getItemViewType(int position) {
        return 1;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        final TagsViewHolder postHeader = (TagsViewHolder) holder;
        if(position==0)
            sCOUNT_MAX = Integer.parseInt(postList.get(0).getmCount());
        postHeader.mName.setText(postList.get(position).getmName());
        postHeader.mCount.setText(postList.get(position).getmCount());
        postHeader.mCountProgress.setMax(sCOUNT_MAX);
        postHeader.mCountProgress.setProgress(Integer.parseInt(postList.get(position).getmCount()));
    }


    public TagsRecyclerAdapter(Context context, List<TagsModel> postList, OnTagsClickListener onItemClickListener) {
        onTagsClickListener = onItemClickListener;
        mImageLoader = VolleySingleton.getInstance().getImageLoader();
        mContext = context;
        mLayoutInflater = LayoutInflater.from(mContext);
        mTimeRelative = new TimeRelative();
        this.postList = postList;
    }


    @Override
    public int getItemCount() {
        return postList.size();
    }
}
