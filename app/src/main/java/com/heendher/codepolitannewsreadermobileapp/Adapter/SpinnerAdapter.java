package com.heendher.codepolitannewsreadermobileapp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.heendher.codepolitannewsreadermobileapp.POJO.SpinnerModel;
import com.heendher.codepolitannewsreadermobileapp.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by -H- on 9/10/2015.
 */
public class SpinnerAdapter extends BaseAdapter {

    private List<SpinnerModel> mItems = new ArrayList<>();
    private Context mContext;

    public SpinnerAdapter(List<SpinnerModel> mItems, Context mContext) {
        this.mItems = mItems;
        this.mContext = mContext;
    }

    public void clear() {
        mItems.clear();
    }

    public void addItem(SpinnerModel yourObject) {
        mItems.add(yourObject);
    }


    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getDropDownView(int position, View view, ViewGroup parent) {
        if (view == null || !view.getTag().toString().equals("DROPDOWN")) {
            view = LayoutInflater.from(mContext).inflate(R.layout.toolbar_spinner_item_dropdown, parent, false);
            view.setTag("DROPDOWN");
        }

        TextView textView = (TextView) view.findViewById(android.R.id.text1);
        textView.setText(getTitle(position));

        return view;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if (view == null || !view.getTag().toString().equals("NON_DROPDOWN")) {
            view = LayoutInflater.from(mContext).inflate(R.layout.
                    toolbar_spinner_item_actionbar, parent, false);
            view.setTag("NON_DROPDOWN");
        }
        TextView textView = (TextView) view.findViewById(android.R.id.text1);
        textView.setText(getTitle(position));
        return view;
    }

    private String getTitle(int position) {
        return position >= 0 && position < mItems.size() ? mItems.get(position).getTitle() : "";
    }
}