package com.heendher.codepolitannewsreadermobileapp.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.heendher.codepolitannewsreadermobileapp.Fragment.MemeFragment;
import com.heendher.codepolitannewsreadermobileapp.Fragment.NyankomikFragment;
import com.heendher.codepolitannewsreadermobileapp.Fragment.QuotesFragment;

/**
 * Created by -H- on 9/9/2015.
 */
public class PagerAdapter extends FragmentStatePagerAdapter {
    String[] title = new String[]{"Page1","Page2","Page3"};
    public PagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment;
        if(position==0){
            fragment = new MemeFragment();
        }else if(position==1){
            fragment = new NyankomikFragment();
        }else{
            fragment = new QuotesFragment();
        }


        return fragment;
    }

    @Override
    public int getCount() {
        return title.length;
    }
}
