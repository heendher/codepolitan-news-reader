package com.heendher.codepolitannewsreadermobileapp.Utils;

import android.content.Context;
import android.text.format.DateUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by -H- on 9/8/2015.
 */
public class TimeRelative {

    private Context mContext;

    public String getRelativeTime(String time, Context context){
        String relativeTime;
        mContext = context;
        relativeTime = (String)DateUtils.getRelativeTimeSpanString(getDateInMillis(time), System.currentTimeMillis(),DateUtils.SECOND_IN_MILLIS);
        //relativeTime = DateUtils.getRelativeDateTimeString(mContext, getDateInMillis(time),
         //       DateUtils.SECOND_IN_MILLIS, DateUtils.WEEK_IN_MILLIS, DateUtils.FORMAT_ABBREV_TIME).toString();
        return relativeTime;


    }

    private long getDateInMillis(String srcDate) {
        SimpleDateFormat desiredFormat = new SimpleDateFormat(
                "yyyy-MM-dd kk:mm:ss");

        long dateInMillis = 0;
        try {
            Date date = desiredFormat.parse(srcDate);
            dateInMillis = date.getTime();
            return dateInMillis;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return 0;
    }
}
