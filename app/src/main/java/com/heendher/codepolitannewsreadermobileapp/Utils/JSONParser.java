package com.heendher.codepolitannewsreadermobileapp.Utils;

import com.heendher.codepolitannewsreadermobileapp.POJO.PostModel;
import com.heendher.codepolitannewsreadermobileapp.POJO.TagsModel;
import com.heendher.codepolitannewsreadermobileapp.Template.Template;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by -H- on 9/8/2015.
 */
public class JSONParser {
    public List<PostModel> Post(List<PostModel> postList, JSONObject jsonObject){
        List<PostModel> mPostList = postList;
        try {
            if (jsonObject == null || jsonObject.length() == 0 || jsonObject.getJSONObject(Template.Global.CODEERROR).equals(Template.Global.CODE)) {
                return mPostList;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            JSONArray jsonArray = jsonObject.getJSONArray(Template.Global.ARRAY);

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject postObject = jsonArray.getJSONObject(i);
                PostModel post = new PostModel();
                post.setId(postObject.getInt(Template.Post.ID));
                post.setSlug(postObject.getString(Template.Post.SLUG));
                post.setTitle(postObject.getString(Template.Post.TITLE));
                post.setExcerpt(postObject.getString(Template.Post.EXCERPT));
                post.setDate(postObject.getString(Template.Post.DATE));
                post.setDateClear(postObject.getString(Template.Post.DATE_CLEAR));
                post.setLink(postObject.getString(Template.Post.LINK));
                post.setThumbnailSmall(postObject.getString(Template.Post.THUMBNAIL_SMALL));
                post.setThumbnailMedium(postObject.getString(Template.Post.THUMBNAIL_MEDIUM));

                mPostList.add(post);
            }


            return mPostList;

        } catch (JSONException e) {
            e.printStackTrace();

        }

        return null;
    }

    public List<TagsModel> Tags(List<TagsModel> postList, JSONObject jsonObject){
        List<TagsModel> mPostList = postList;
        try {
            if (jsonObject == null || jsonObject.length() == 0 || jsonObject.getJSONObject(Template.Global.CODEERROR).equals(Template.Global.CODE)) {
                return mPostList;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            JSONArray jsonArray = jsonObject.getJSONArray(Template.Global.ARRAY);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject postObject = jsonArray.getJSONObject(i);
                TagsModel post = new TagsModel();
                post.setmCount(postObject.getString(Template.Tags.COUNT));
                post.setmName(postObject.getString(Template.Tags.NAME));
                post.setmSlug(postObject.getString(Template.Tags.SLUG));

                mPostList.add(post);
            }


            return mPostList;

        } catch (JSONException e) {
            e.printStackTrace();

        }

        return null;
    }


}
