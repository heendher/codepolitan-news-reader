package com.heendher.codepolitannewsreadermobileapp.Utils;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.support.v7.widget.RecyclerView;

/**
 * Created by -H- on 9/27/2015.
 */
public class AnimationUtil {

    public static void animate(RecyclerView.ViewHolder holder, boolean goesDown){
        AnimatorSet animationSet = new AnimatorSet();
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(holder.itemView, "translationY", goesDown==true? 200:-200,0);
        objectAnimator.setDuration(1000);
        animationSet.playTogether(objectAnimator);
        animationSet.start();
    }
}
