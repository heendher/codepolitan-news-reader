package com.heendher.codepolitannewsreadermobileapp.Utils;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.heendher.codepolitannewsreadermobileapp.Application.MyApplication;

/**
 * Created by -H- on 9/6/2015.
 */
public class VolleySingleton {

    private static VolleySingleton sInstance = null;
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;

    private VolleySingleton(){
        mRequestQueue = Volley.newRequestQueue(MyApplication.getContext());
    }



    public static VolleySingleton getInstance() {
        if(sInstance == null){
            sInstance = new VolleySingleton();
        }
        return sInstance;
    }

    public RequestQueue getRequestQueue(){
        return mRequestQueue;
    }

    public ImageLoader getImageLoader(){
        if(mImageLoader==null){
            mImageLoader = new ImageLoader(mRequestQueue,new LruBitmapCache());
        }
        return mImageLoader;
    }
}
