package com.heendher.codepolitannewsreadermobileapp.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.heendher.codepolitannewsreadermobileapp.Activity.DetailActivity;
import com.heendher.codepolitannewsreadermobileapp.Activity.HomeActivity;
import com.heendher.codepolitannewsreadermobileapp.Activity.SearchResultActivity;
import com.heendher.codepolitannewsreadermobileapp.Activity.TagsActivity;
import com.heendher.codepolitannewsreadermobileapp.R;
import com.heendher.codepolitannewsreadermobileapp.Template.Template;

/**
 * A simple {@link Fragment} subclass.
 */
public class ErrorFragment extends Fragment implements GestureDetector.OnGestureListener{

    private TextView mErrorText;
    private ImageView mErrorLogo;
    private String mLastAccess;
    private GestureDetector mGestureDetector;
    private String mGetActivity;
    public ErrorFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_error, container, false);
        mGestureDetector = new GestureDetector(getActivity(),this);
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                return mGestureDetector.onTouchEvent(motionEvent);
            }
        });
        mErrorText = (TextView)view.findViewById(R.id.textConnectionError);
        mErrorLogo = (ImageView)view.findViewById(R.id.imageConnectionError);
        mLastAccess = getArguments().getString("last_access");
        mGetActivity = getArguments().getString("get_activity");

        if(getArguments().getString("error").equals(Template.ConnectionProblem.INTERNETERROR)){
            mErrorText.setText(Template.ConnectionProblem.MSGINTERNET);
            mErrorLogo.setImageResource(R.drawable.ic_portable_wifi_off_white_48dp);
        }else{
            mErrorText.setText(Template.ConnectionProblem.MSGSERVER);
            mErrorLogo.setImageResource(R.drawable.ic_cloud_off_white_48dp);
        }
        return view;
    }




    @Override
    public boolean onDown(MotionEvent motionEvent) {
        return true;
    }

    @Override
    public void onShowPress(MotionEvent motionEvent) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent motionEvent) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent motionEvent) {

    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float v, float v1) {
        if (e1.getY() < e2.getY()) {
            if(mGetActivity.equals(Template.Activity.MAIN))
            ((HomeActivity)getActivity()).setFragmentLayout(mLastAccess,"");
            else if(mGetActivity.equals(Template.Activity.SEARCH_RESULT))
                ((SearchResultActivity)getActivity()).setFragmentLayout(mLastAccess,"");
            else if(mGetActivity.equals(Template.Activity.DETAIL))
                ((DetailActivity)getActivity()).setFragmentLayout(mLastAccess,"");
            else if(mGetActivity.equals(Template.Activity.TAGS_ACTIVITY))
                ((TagsActivity)getActivity()).setFragmentLayout(mLastAccess,"");
        }
        return true;
    }
}
