package com.heendher.codepolitannewsreadermobileapp.Fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.heendher.codepolitannewsreadermobileapp.Activity.DetailActivity;
import com.heendher.codepolitannewsreadermobileapp.Activity.TagsActivity;
import com.heendher.codepolitannewsreadermobileapp.Adapter.PostRecyclerAdapter;
import com.heendher.codepolitannewsreadermobileapp.Interface.OnPostClickListener;
import com.heendher.codepolitannewsreadermobileapp.Interface.OnSpinnerClickListener;
import com.heendher.codepolitannewsreadermobileapp.POJO.PostModel;
import com.heendher.codepolitannewsreadermobileapp.R;
import com.heendher.codepolitannewsreadermobileapp.Template.EndpointAPI;
import com.heendher.codepolitannewsreadermobileapp.Template.Template;
import com.heendher.codepolitannewsreadermobileapp.Utils.JSONParser;
import com.heendher.codepolitannewsreadermobileapp.Utils.VolleySingleton;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class TagsResultFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, OnPostClickListener, OnSpinnerClickListener {


    private PostRecyclerAdapter mPostAdapter;
    private RecyclerView mRecyclerView;
    private RequestQueue mRequestQueue;
    private List<PostModel> mPostList;
    private int mCurrentPage = 1;
    private boolean mLoading = true;
    private int mPastVisiblesItems, mVisibleItemCount, mTotalItemCount;
    private SwipeRefreshLayout mSwipe;
    private GridLayoutManager manager;
    private JSONParser mJsonParser = new JSONParser();
    private String mKategori;
    private String mSlug, mTag;

    public TagsResultFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mKategori = Template.PostType.MEME;
        mPostList = new ArrayList<>();
        mRequestQueue = VolleySingleton.getInstance().getRequestQueue();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_post, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycle_home);
        mSwipe = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh);
        mSlug = getArguments().getString("slug");
        mTag = getArguments().getString("type");
        manager = new GridLayoutManager(getContext(), 2);
        mRecyclerView.setLayoutManager(manager);
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                mVisibleItemCount = manager.getChildCount();
                mTotalItemCount = manager.getItemCount();
                mPastVisiblesItems = manager.findFirstVisibleItemPosition();

                if (mLoading) {
                    if ((mVisibleItemCount + mPastVisiblesItems) >= mTotalItemCount) {
                        mLoading = false;
                        sendJSONLoadMore(mTag);
                    }
                }
            }
        });


        mSwipe.setOnRefreshListener(this);
        mSwipe.setColorSchemeResources(R.color.accentColor, R.color.accentColor, R.color.accentColor, R.color.accentColor);
        mSwipe.post(new Runnable() {
            @Override
            public void run() {
                sendJSONReset(mTag);
            }
        });
        return view;
    }


    String getQueryKategori(String tag) {
        if (tag.equals("tags"))
            return EndpointAPI.POST_TAG + mSlug + "/";
        else
            return EndpointAPI.KATEGORI + mSlug + "/";
    }

    void resetData() {
        mCurrentPage = 1;
        mPostList.clear();
        mPostList = new ArrayList<>();
    }

    void sendJSONReset(String tag) {

        String queryKategori = getQueryKategori(tag);
        resetData();
        mSwipe.setRefreshing(true);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                queryKategori + mCurrentPage,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        mRecyclerView.setAdapter(null);
                        mPostAdapter = new PostRecyclerAdapter(getActivity(), mJsonParser.Post(mPostList, response), TagsResultFragment.this, PostRecyclerAdapter.TYPE_KOMIK);
                        mRecyclerView.setAdapter(mPostAdapter);
                        mRecyclerView.setLayoutManager(manager);
                        mPostAdapter.notifyDataSetChanged();
                        mCurrentPage++;
                        mLoading = true;
                        mSwipe.setRefreshing(false);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mLoading = true;

                mSwipe.setRefreshing(false);
                if (error instanceof TimeoutError || error instanceof NoConnectionError || error instanceof NetworkError) {
                    ((TagsActivity) getActivity()).setFragmentLayout("error", Template.ConnectionProblem.INTERNETERROR);
                } else {
                    ((TagsActivity) getActivity()).setFragmentLayout("error", Template.ConnectionProblem.SERVERERROR);
                }


            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(Template.VolleyRetryPolicy.SOCKET_TIMEOUT,
                Template.VolleyRetryPolicy.RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        mRequestQueue.add(jsonObjectRequest);


    }

    void sendJSONLoadMore(String title) {

        String queryKategori = getQueryKategori(title);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                queryKategori + mCurrentPage,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        mPostList = mJsonParser.Post(mPostList, response);


                        mPostAdapter.notifyDataSetChanged();

                        mCurrentPage++;
                        mLoading = true;

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mLoading = true;
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(Template.VolleyRetryPolicy.SOCKET_TIMEOUT,
                Template.VolleyRetryPolicy.RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        mRequestQueue.add(jsonObjectRequest);


    }

    @Override
    public void onRefresh() {
        sendJSONReset(mTag);

    }

    @Override
    public void OnClickListener(PostModel post) {
        Intent intent = new Intent(getActivity(), DetailActivity.class);
        intent.putExtra("id", post.getId());
        intent.putExtra("image_id", post.getThumbnailMedium());
        intent.putExtra("title", post.getTitle());
        intent.putExtra("excerpt", post.getExcerpt());
        intent.putExtra("link", post.getLink());
        intent.putExtra("date",post.getDateClear());
        startActivity(intent);
        getActivity().finish();

    }

    @Override
    public void OnShareClickListener(String subject, String content) {
        ((TagsActivity) getActivity()).shareIt(subject, content);
    }

    @Override
    public void OnItemClickListener(String title) {
        sendJSONReset(mKategori);
    }
}
