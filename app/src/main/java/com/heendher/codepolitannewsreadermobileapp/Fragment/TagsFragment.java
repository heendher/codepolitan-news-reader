package com.heendher.codepolitannewsreadermobileapp.Fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.heendher.codepolitannewsreadermobileapp.Activity.HomeActivity;
import com.heendher.codepolitannewsreadermobileapp.Activity.TagsActivity;
import com.heendher.codepolitannewsreadermobileapp.Adapter.TagsRecyclerAdapter;
import com.heendher.codepolitannewsreadermobileapp.Interface.OnSpinnerClickListener;
import com.heendher.codepolitannewsreadermobileapp.Interface.OnTagsClickListener;
import com.heendher.codepolitannewsreadermobileapp.POJO.TagsModel;
import com.heendher.codepolitannewsreadermobileapp.R;
import com.heendher.codepolitannewsreadermobileapp.Template.EndpointAPI;
import com.heendher.codepolitannewsreadermobileapp.Template.Template;
import com.heendher.codepolitannewsreadermobileapp.Utils.JSONParser;
import com.heendher.codepolitannewsreadermobileapp.Utils.VolleySingleton;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class TagsFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener,OnTagsClickListener, OnSpinnerClickListener {

    private TagsRecyclerAdapter mPostAdapter;
    private RecyclerView mRecyclerView;
    private RequestQueue mRequestQueue;
    private List<TagsModel> mPostList;
    private int mCurrentPage = 1;
    private boolean mLoading = true;
    private int mPastVisiblesItems, mVisibleItemCount, mTotalItemCount;
    private LinearLayoutManager manager;
    private SwipeRefreshLayout mSwipe;
    private StaggeredGridLayoutManager staggeredGridLayoutManager;
    private JSONParser mJsonParser = new JSONParser();
    private String mKategori;
    private boolean isReady = false, isReadyToRun = false;
    private JsonObjectRequest mJsonObjectRequest;
    public TagsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mKategori = Template.KategoriName.ALLPOSTS;
        mPostList = new ArrayList<>();
        mRequestQueue = VolleySingleton.getInstance().getRequestQueue();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        ((HomeActivity) getActivity()).setSpinnerClickListener(this);
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_post, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycle_home);
        mSwipe = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh);

        manager = new LinearLayoutManager(getContext());

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                mVisibleItemCount = manager.getChildCount();
                mTotalItemCount = manager.getItemCount();
                mPastVisiblesItems = manager.findFirstVisibleItemPosition();

                if (mLoading) {
                    if ((mVisibleItemCount + mPastVisiblesItems) >= mTotalItemCount) {
                        mLoading = false;
                        sendJSONLoadMore(mKategori);
                    }
                }
            }
        });

        mRecyclerView.setLayoutManager(manager);
        mSwipe.setOnRefreshListener(this);
        mSwipe.setColorSchemeResources(R.color.accentColor, R.color.accentColor, R.color.accentColor, R.color.accentColor);
        mSwipe.post(new Runnable() {
            @Override
            public void run() {


                    sendJSONReset(Template.KategoriName.ALLPOSTS);



            }
        });
        return view;
    }


    String getQueryKategori(String title) {
        return EndpointAPI.TAGS;
    }

    void resetData() {
        mPostList.clear();
        mPostList = new ArrayList<>();
        mCurrentPage = 1;

    }

    void sendJSONReset(String title) {

        String queryKategori = getQueryKategori(title);
        resetData();


        mSwipe.setRefreshing(true);
        mJsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                queryKategori + mCurrentPage,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("ResponeJSON", response + "");
                        mRecyclerView.setAdapter(null);
                        mPostAdapter = new TagsRecyclerAdapter(getContext(),mJsonParser.Tags(mPostList, response),TagsFragment.this);
                        mRecyclerView.setAdapter(mPostAdapter);
                        mRecyclerView.setLayoutManager(manager);
                        mRecyclerView.setVisibility(RecyclerView.VISIBLE);
                        mPostAdapter.notifyDataSetChanged();
                        mCurrentPage++;
                        mLoading = true;
                        mSwipe.setRefreshing(false);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mLoading = true;

                mSwipe.setRefreshing(false);
                if(error instanceof TimeoutError || error instanceof NoConnectionError || error instanceof NetworkError){
                    ((HomeActivity) getActivity()).setFragmentLayout("error",Template.ConnectionProblem.INTERNETERROR);
                }else{
                    ((HomeActivity) getActivity()).setFragmentLayout("error",Template.ConnectionProblem.SERVERERROR);
                }


            }
        });
        mJsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(Template.VolleyRetryPolicy.SOCKET_TIMEOUT,
                Template.VolleyRetryPolicy.RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        mRequestQueue.add(mJsonObjectRequest);


    }

    void sendJSONLoadMore(String title) {

        String queryKategori = getQueryKategori(title);
        mJsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                queryKategori + mCurrentPage,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        if (mJsonParser.Tags(mPostList, response) != null) {
                            mPostList = mJsonParser.Tags(mPostList, response);
                            mPostAdapter.notifyDataSetChanged();

                            mCurrentPage++;
                        }

                        mLoading = true;

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mLoading = true;
            }
        });

        mJsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(Template.VolleyRetryPolicy.SOCKET_TIMEOUT,
                Template.VolleyRetryPolicy.RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        mRequestQueue.add(mJsonObjectRequest);


    }

    @Override
    public void onStop() {
        mRequestQueue.cancelAll(mJsonObjectRequest);
        mJsonObjectRequest.cancel();
        mSwipe.setRefreshing(false);
        super.onStop();
    }



    @Override
    public void onRefresh() {
        sendJSONReset(mKategori);

    }


    @Override
    public void OnItemClickListener(String title) {

        isReadyToRun = true;
        if(isReady){
            mKategori = title;
            sendJSONReset(mKategori);
        }

    }

    @Override
    public void onClickListener(TagsModel tag) {
        Intent intent = new Intent(getActivity(), TagsActivity.class);
        intent.putExtra("slug", tag.getmSlug());
        intent.putExtra("type", "tags");
        startActivity(intent);
    }
}
