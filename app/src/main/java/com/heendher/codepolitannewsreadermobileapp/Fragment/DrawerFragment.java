package com.heendher.codepolitannewsreadermobileapp.Fragment;


import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.heendher.codepolitannewsreadermobileapp.Activity.HomeActivity;
import com.heendher.codepolitannewsreadermobileapp.Adapter.DrawerRecyclerAdapter;
import com.heendher.codepolitannewsreadermobileapp.POJO.DrawerModel;
import com.heendher.codepolitannewsreadermobileapp.R;
import com.heendher.codepolitannewsreadermobileapp.Template.Template;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class DrawerFragment extends Fragment implements DrawerRecyclerAdapter.OnClickListener {


    private RecyclerView mRecyclerView;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private Toolbar mToolbar;
    private DrawerRecyclerAdapter mAdapterSG;
    private boolean mIsDrawerOpen = false;

    public DrawerFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_drawer, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.drawer_recycler);
        //mDrawerAdapter = new DrawerRecyclerAdapter(getActivity(), getData());
        mAdapterSG = new DrawerRecyclerAdapter(getActivity(), getData(), this);
        mRecyclerView.setAdapter(mAdapterSG);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        return view;

    }

    List<DrawerModel> getData() {
        List<DrawerModel> items = new ArrayList<>();
        items.add(new DrawerModel(true));
        items.add(new DrawerModel(R.drawable.ic_dashboard_white_24dp, R.drawable.ic_dashboard_teal_500_24dp, Template.Drawer.HOME));
        items.add(new DrawerModel(R.drawable.ic_label_white_24dp, R.drawable.ic_label_teal_500_24dp, Template.Drawer.TAGS));
        items.add(new DrawerModel(true));
        items.add(new DrawerModel("Channels"));
        items.add(new DrawerModel(R.drawable.ic_account_box_white_24dp, R.drawable.ic_account_box_teal_500_24dp, Template.Drawer.NYANKOMIK));
        items.add(new DrawerModel(R.drawable.ic_tag_faces_white_24dp, R.drawable.ic_tag_faces_teal_500_24dp, Template.Drawer.MEME));
        items.add(new DrawerModel(R.drawable.ic_format_quote_white_24dp, R.drawable.ic_format_quote_teal_500_24dp, Template.Drawer.QUOTES));
        items.add(new DrawerModel(true));
        items.add(new DrawerModel("Our Networks"));
        items.add(new DrawerModel(R.drawable.ic_thumb_up_white_24dp, R.drawable.ic_thumb_up_teal_500_24dp, Template.Drawer.KARYALOKAL));
        items.add(new DrawerModel(R.drawable.ic_format_list_bulleted_white_24dp, R.drawable.ic_format_list_bulleted_teal_500_24dp, Template.Drawer.NYANKODMAGZ));
        items.add(new DrawerModel(R.drawable.ic_local_library_white_24dp, R.drawable.ic_local_library_teal_500_24dp, Template.Drawer.PUSAKACMS));
        items.add(new DrawerModel(true));
        items.add(new DrawerModel("More"));
        items.add(new DrawerModel(R.drawable.ic_info_white_24dp, R.drawable.ic_info_teal_500_24dp, Template.Drawer.TENTANG));
        items.add(new DrawerModel(R.drawable.ic_forum_white_24dp, R.drawable.ic_forum_teal_500_24dp, Template.Drawer.PARTNERS));
        items.add(new DrawerModel(R.drawable.ic_call_white_24dp, R.drawable.ic_call_teal_500_24dp, Template.Drawer.CONTACTUS));

        return items;
    }

    public void setUp(DrawerLayout drawerLayout, Toolbar toolbar) {
        mDrawerLayout = drawerLayout;
        mToolbar = toolbar;
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), mDrawerLayout, mToolbar, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                setDrawerStatus(true);
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                setDrawerStatus(false);
                getActivity().invalidateOptionsMenu();
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });
    }

    public void setDrawerIndicator(boolean onOff) {

        if (onOff) {
            mDrawerToggle = new ActionBarDrawerToggle(getActivity(), mDrawerLayout, mToolbar, R.string.drawer_open, R.string.drawer_close) {
                @Override
                public void onDrawerOpened(View drawerView) {
                    super.onDrawerOpened(drawerView);
                    setDrawerStatus(true);
                    getActivity().invalidateOptionsMenu();
                }

                @Override
                public void onDrawerClosed(View drawerView) {
                    super.onDrawerClosed(drawerView);
                    setDrawerStatus(false);
                    getActivity().invalidateOptionsMenu();
                }
            };
            mDrawerLayout.setDrawerListener(null);
            mDrawerLayout.setDrawerListener(mDrawerToggle);
            mDrawerLayout.post(new Runnable() {
                @Override
                public void run() {
                    mDrawerToggle.syncState();
                }
            });
        }
        mDrawerToggle.setDrawerIndicatorEnabled(onOff);

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        mDrawerToggle.onConfigurationChanged(newConfig);
        super.onConfigurationChanged(newConfig);
    }

    public void setDrawerStatus(boolean isDrawerOpen) {
        mIsDrawerOpen = isDrawerOpen;
    }

    public void closeDrawer() {
        mDrawerLayout.closeDrawer(GravityCompat.START);
        setDrawerStatus(false);
    }

    public boolean getDrawerStatus() {
        return mIsDrawerOpen;
    }

    @Override
    public void OnItemClickListener(String title, int position) {
        //Toast.makeText(getContext(),"" + position, Toast.LENGTH_SHORT).show();
        DrawerRecyclerAdapter.SELECTED_ITEM = position;
        mRecyclerView.getAdapter().notifyDataSetChanged();
        ((HomeActivity) getActivity()).setFragmentLayout(title, "");
        closeDrawer();

    }
}
