package com.heendher.codepolitannewsreadermobileapp.Fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.heendher.codepolitannewsreadermobileapp.Activity.DetailActivity;
import com.heendher.codepolitannewsreadermobileapp.Activity.SearchResultActivity;
import com.heendher.codepolitannewsreadermobileapp.Adapter.PostRecyclerAdapter;
import com.heendher.codepolitannewsreadermobileapp.Interface.OnPostClickListener;
import com.heendher.codepolitannewsreadermobileapp.POJO.PostModel;
import com.heendher.codepolitannewsreadermobileapp.R;
import com.heendher.codepolitannewsreadermobileapp.Template.EndpointAPI;
import com.heendher.codepolitannewsreadermobileapp.Template.Template;
import com.heendher.codepolitannewsreadermobileapp.Utils.CustomRequest;
import com.heendher.codepolitannewsreadermobileapp.Utils.JSONParser;
import com.heendher.codepolitannewsreadermobileapp.Utils.VolleySingleton;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchResultFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, OnPostClickListener {


    private PostRecyclerAdapter mPostAdapter;
    private RecyclerView mRecyclerView;
    private RequestQueue mRequestQueue;
    private List<PostModel> mPostList;
    private int mCurrentPage = 1;
    private boolean mLoading = true;
    private int mPastVisiblesItems, mVisibleItemCount, mTotalItemCount;
    private SwipeRefreshLayout mSwipe;
    private GridLayoutManager manager;
    private JSONParser mJsonParser = new JSONParser();
    private String mQuery;
    private CustomRequest mCustomRequest;

    public SearchResultFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPostList = new ArrayList<>();
        mRequestQueue = VolleySingleton.getInstance().getRequestQueue();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_post, container, false);

        mQuery = getArguments().getString("Query");
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycle_home);
        mSwipe = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh);

        manager = new GridLayoutManager(getContext(), 2);

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                mVisibleItemCount = manager.getChildCount();
                mTotalItemCount = manager.getItemCount();
                mPastVisiblesItems = manager.findFirstVisibleItemPosition();

                if (mLoading) {
                    if ((mVisibleItemCount + mPastVisiblesItems) >= mTotalItemCount) {
                        mLoading = false;
                        sendJSONLoadMore();
                    }
                }
            }
        });
        mRecyclerView.setLayoutManager(manager);

        mSwipe.setOnRefreshListener(this);
        mSwipe.setColorSchemeResources(R.color.accentColor, R.color.accentColor, R.color.accentColor, R.color.accentColor);
        mSwipe.post(new Runnable() {
            @Override
            public void run() {
                sendJSONReset();
            }
        });
        return view;
    }


    void resetData() {
        mCurrentPage = 1;
        mPostList.clear();
        mPostList = new ArrayList<>();

    }

    void sendJSONReset() {

        mSwipe.setRefreshing(true);
        Map<String, String> params = new HashMap<>();
        params.put("keyword", mQuery);
        params.put("page", 1 + "");

        mCustomRequest = new CustomRequest(Request.Method.POST, EndpointAPI.SEARCH, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                resetData();
                    mRecyclerView.setAdapter(null);
                    mPostList = mJsonParser.Post(mPostList, response);
                    if(mPostList == null){
                        Toast.makeText(getContext(),"Oops, kata kunci yang ada cari tidak ditemukan",Toast.LENGTH_LONG).show();
                    }else{
                        mPostAdapter = new PostRecyclerAdapter(getActivity(), mPostList, SearchResultFragment.this, PostRecyclerAdapter.TYPE_KOMIK);
                        mRecyclerView.setAdapter(mPostAdapter);
                        mRecyclerView.setLayoutManager(manager);
                        mPostAdapter.notifyDataSetChanged();
                        mCurrentPage++;

                    }


                mLoading = true;
                mSwipe.setRefreshing(false);



            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mLoading = true;

                mSwipe.setRefreshing(false);
                if(error instanceof TimeoutError || error instanceof NoConnectionError || error instanceof NetworkError){
                    ((SearchResultActivity) getActivity()).setFragmentLayout("error", Template.ConnectionProblem.INTERNETERROR);
                }else{
                    ((SearchResultActivity) getActivity()).setFragmentLayout("error",Template.ConnectionProblem.SERVERERROR);
                }

            }
        });
        mRequestQueue.add(mCustomRequest);
    }

    void sendJSONLoadMore() {
        Map<String, String> params = new HashMap<>();
        params.put("keyword", mQuery);
        params.put("page", mCurrentPage + "");

        mCustomRequest = new CustomRequest(Request.Method.POST, EndpointAPI.SEARCH, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if(mJsonParser.Post(mPostList,response)!=null){
                    mPostList = mJsonParser.Post(mPostList, response);
                    mPostAdapter.notifyDataSetChanged();

                    mCurrentPage++;
                }

                mLoading = true;


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mLoading = true;

            }
        });
        mRequestQueue.add(mCustomRequest);
    }

    @Override
    public void onStop() {
        mRequestQueue.cancelAll(mCustomRequest);
        mCustomRequest.cancel();
        mSwipe.setRefreshing(false);
        super.onStop();
    }

    @Override
    public void onRefresh() {
        sendJSONReset();

    }

    @Override
    public void OnClickListener(PostModel post) {
        Intent intent = new Intent(getActivity(), DetailActivity.class);
        intent.putExtra("id", post.getId());
        intent.putExtra("image_id", post.getThumbnailMedium());
        intent.putExtra("title",post.getTitle());
        intent.putExtra("excerpt",post.getExcerpt());
        intent.putExtra("link",post.getLink());
        intent.putExtra("date",post.getDate());
        startActivity(intent);
    }

    @Override
    public void OnShareClickListener(String subject, String content) {
        ((SearchResultActivity)getActivity()).shareIt(subject,content);
    }


}
