package com.heendher.codepolitannewsreadermobileapp.Fragment;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.heendher.codepolitannewsreadermobileapp.Activity.HomeActivity;
import com.heendher.codepolitannewsreadermobileapp.R;
import com.heendher.codepolitannewsreadermobileapp.Template.Template;

/**
 * A simple {@link Fragment} subclass.
 */
public class OurNetworkFragment extends Fragment implements View.OnClickListener{


    private ImageView mImageHeader, mShare, mFacebook, mTwitter, mWeb;
    private TextView mTitle, mContent;
    private String mOurtNetwork, mFacebookURL, mTwitterURL, mWebURL, mShareTitle, mShareContent;
    private RelativeLayout mBackground;

    public OurNetworkFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_our_network, container, false);
        mOurtNetwork = getArguments().getString("our_network");
        mBackground = (RelativeLayout) view.findViewById(R.id.backgroundOurNetwork);
        mImageHeader = (ImageView) view.findViewById(R.id.imageOurNetwork);
        mShare = (ImageView) view.findViewById(R.id.imageShare);
        mShare.setOnClickListener(this);
        mFacebook = (ImageView) view.findViewById(R.id.imageFacebook);
        mFacebook.setOnClickListener(this);
        mTwitter = (ImageView) view.findViewById(R.id.imageTwitter);
        mTwitter.setOnClickListener(this);
        mWeb = (ImageView) view.findViewById(R.id.imageWeb);
        mWeb.setOnClickListener(this);
        mTitle = (TextView) view.findViewById(R.id.title);
        mContent = (TextView) view.findViewById(R.id.content);
        switch (mOurtNetwork) {
            case Template.Drawer.KARYALOKAL:
                mTitle.setText("Karya Lokal");
                mContent.setText("Karyalokal CodePolitan adalah website direktori yang berisi karya-karya " +
                        "atau produk-produk yang dihasilkan oleh programmer lokal. Karya dan produk tersebut " +
                        "dapat berupa development tools, library, plugin, aplikasi, multimedia pembelajaran, cms, " +
                        "distro linux, game, anti virus, web apps dan karya-karya lainnya yang berguna baik itu bagi " +
                        "user ataupun bagi para developer.\n" +
                        "\n" +
                        "\n" +
                        "KaryaLokal CodePolitan dibangun oleh CodePolitan untuk mewadahi para programmer lokal yang " +
                        "memiliki karya untuk mempublikasikan karyanya kepada publik. Dengan adanya sarana publikasi " +
                        "untuk programmer ini diharapkan para programmer lokal dapat lebih bersemangat dalam berkarya " +
                        "dan menghasilkan produknya.");
                mTwitter.setVisibility(ImageView.GONE);
                mFacebookURL = "https://www.facebook.com/KaryaLokal-704440339650948/";
                mWebURL = "http://www.karyalokal.com/";
                mShareContent = mWebURL;
                mShareTitle = "Karya Lokal";
                break;
            case Template.Drawer.NYANKODMAGZ:
                mImageHeader.setImageResource(R.drawable.nyankodmagz);
                mTitle.setText("nyankodMagz");
                mContent.setText("Belajar pemrograman emang nggak gampang, tapi bukan berarti " +
                        "harus dibikin susah juga. Belajar pemrograman bisa menyenangkan jika kita " +
                        "membuatnya menyenangkan. Di nyankodMagz Kamu akan melihat cara belajar " +
                        "pemrograman yang lebih sederhana, nyaman, jenaka dan konyol, yang bakal " +
                        "bikin belajar pemrograman jadi nggak ngebosenin.");
                mFacebookURL = "https://www.facebook.com/nyankod";
                mWebURL = "http://www.nyankod.com/";
                mShareContent = mWebURL;
                mShareTitle = "nyankodMagz";
                mTwitterURL = "https://twitter.com/nyankodtweet";
                break;
            case Template.Drawer.PUSAKACMS:
                mImageHeader.setImageResource(R.drawable.pusakacms);
                mTitle.setText("PusakaCMS");
                mContent.setText("PusakaCMS adalah Content Management System (CMS) dengan konten " +
                        "berbasis file sehingga tidak memerlukan instalasi database server untuk " +
                        "penyimpanan data. Cukup tulis kontenmu di komputer kemudian unggah, dan " +
                        "kontenmu bisa langsung diakses oleh seluruh dunia!");
                mFacebookURL = "https://www.facebook.com/pusakacms";
                mWebURL = "http://www.pusakacms.org/";
                mShareContent = mWebURL;
                mShareTitle = "PusakaCMS";
                mTwitter.setVisibility(ImageView.GONE);
                break;
        }
        return view;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imageFacebook:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(mFacebookURL)));
                break;
            case R.id.imageTwitter:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(mTwitterURL)));
                break;
            case R.id.imageWeb:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(mWebURL)));
                break;
            case R.id.imageShare:
                ((HomeActivity)getActivity()).shareIt(mShareTitle,mShareContent);
        }
    }

}
