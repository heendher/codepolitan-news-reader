package com.heendher.codepolitannewsreadermobileapp.Fragment;


import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LevelListDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.heendher.codepolitannewsreadermobileapp.Activity.DetailActivity;
import com.heendher.codepolitannewsreadermobileapp.Activity.TagsActivity;
import com.heendher.codepolitannewsreadermobileapp.R;
import com.heendher.codepolitannewsreadermobileapp.Template.EndpointAPI;
import com.heendher.codepolitannewsreadermobileapp.Template.Template;
import com.heendher.codepolitannewsreadermobileapp.Utils.TimeRelative;
import com.heendher.codepolitannewsreadermobileapp.Utils.VolleySingleton;
import com.manuelpeinado.fadingactionbar.view.ObservableScrollable;
import com.manuelpeinado.fadingactionbar.view.OnScrollChangedCallback;
import com.readystatesoftware.systembartint.SystemBarTintManager;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetailFragment extends Fragment implements Html.ImageGetter, OnScrollChangedCallback, View.OnClickListener {


    private ImageView mDetailImage;
    private TextView mTitle, mExcerpt, mDate, mContent, mKategori;
    private LinearLayout mTagsContainer;
    private int mId;
    private RequestQueue mRequestQueue;
    private TimeRelative mTimeRelative;
    private ImageView mImageContent;
    private Drawable mActionBarBackgroundDrawable;
    private Toolbar mToolbar;
    private View mHeader;
    private int mLastDampedScroll;
    private int mInitialStatusBarColor;
    private int mFinalStatusBarColor;
    private SystemBarTintManager mStatusBarManager;
    private JSONArray mTagsArray;
    private LinearLayout mContentLayout;
    private String mTag, mTitleString, mDateString, mImage;
    private RelativeLayout mHeaderContainer;

    public DetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTimeRelative = new TimeRelative();
        mRequestQueue = VolleySingleton.getInstance().getRequestQueue();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail, container, false);
        mToolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        mActionBarBackgroundDrawable = mToolbar.getBackground();
        mStatusBarManager = new SystemBarTintManager(getActivity());
        mStatusBarManager.setStatusBarTintEnabled(true);
        mInitialStatusBarColor = Color.BLACK;
        mFinalStatusBarColor = getResources().getColor(R.color.primaryColor);

        mHeader = view.findViewById(R.id.imageDetail);

        ObservableScrollable scrollView = (ObservableScrollable) view.findViewById(R.id.scrollview);
        scrollView.setOnScrollChangedCallback(this);
        mHeaderContainer = (RelativeLayout) view.findViewById(R.id.headerContainer);
        mContentLayout = (LinearLayout) view.findViewById(R.id.contentLayout);
        mKategori = (TextView) view.findViewById(R.id.detailKategori);
        mDetailImage = (ImageView) view.findViewById(R.id.imageDetail);
        //RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 300);
        //mDetailImage.setLayoutParams(layoutParams);
        mTitle = (TextView) view.findViewById(R.id.titleDetail);
        mDate = (TextView) view.findViewById(R.id.detailDate);
        mContent = (TextView) view.findViewById(R.id.detailContent);
        mTagsContainer = (LinearLayout) view.findViewById(R.id.tagsContainer);
        mId = getArguments().getInt("id");
        mTitleString = getArguments().getString("title");
        mDateString = getArguments().getString("date");
        mImage = getArguments().getString("image_id");
        mTitle.setText(mTitleString);
        mDate.setText(mDateString);
        sendJSON(mId);
        onScroll(-1, 0);
        return view;
    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void updateActionBarTransparency(float scrollRatio) {
        int newAlpha = (int) (scrollRatio * 255);
        mActionBarBackgroundDrawable.setAlpha(newAlpha);
        int sdk = android.os.Build.VERSION.SDK_INT;
        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            mToolbar.setBackgroundDrawable(mActionBarBackgroundDrawable);
        } else {
            mToolbar.setBackground(mActionBarBackgroundDrawable);
        }

    }

    private void updateStatusBarColor(float scrollRatio) {
        int r = interpolate(Color.red(mInitialStatusBarColor), Color.red(mFinalStatusBarColor), 1 - scrollRatio);
        int g = interpolate(Color.green(mInitialStatusBarColor), Color.green(mFinalStatusBarColor), 1 - scrollRatio);
        int b = interpolate(Color.blue(mInitialStatusBarColor), Color.blue(mFinalStatusBarColor), 1 - scrollRatio);
        mStatusBarManager.setTintColor(Color.rgb(r, g, b));
    }

    private void updateParallaxEffect(int scrollPosition) {
        float damping = 0.5f;
        int dampedScroll = (int) (scrollPosition * damping);
        int offset = mLastDampedScroll - dampedScroll;
        mHeader.offsetTopAndBottom(-offset);

        mLastDampedScroll = dampedScroll;
    }

    private int interpolate(int from, int to, float param) {
        return (int) (from * param + to * (1 - param));
    }

    @Override
    public void onScroll(int l, int scrollPosition) {
        Log.d("HeaderHeight", mHeader.getHeight() + "");
        int headerHeight = mHeader.getHeight() - mToolbar.getHeight();
        float ratio = 0;
        if (scrollPosition > 0 && headerHeight > 0) {
            ratio = (float) Math.min(Math.max(scrollPosition, 0), headerHeight) / headerHeight;
        }
        if (ratio == 1) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(true);
        } else {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        updateActionBarTransparency(ratio);
        updateStatusBarColor(ratio);
        updateParallaxEffect(scrollPosition);
    }

    void sendJSON(int id) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                EndpointAPI.DETAIL + id,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseJSON(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (error instanceof TimeoutError || error instanceof NoConnectionError || error instanceof NetworkError) {
                    ((DetailActivity) getActivity()).setFragmentLayout("error", Template.ConnectionProblem.INTERNETERROR);
                } else {
                    ((DetailActivity) getActivity()).setFragmentLayout("error", Template.ConnectionProblem.SERVERERROR);
                }


            }
        });


        mRequestQueue.add(jsonObjectRequest);


    }

    void tagsTextView(int count) throws JSONException {
        for (int i = 0; i < count; i++) {
            TextView textView = new TextView(getContext());
            textView.setText(mTagsArray.getJSONObject(i).getString("name"));
            textView.setBackgroundColor(getResources().getColor(R.color.accentColor));
            textView.setPadding(8, 8, 8, 8);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(8, 0, 8, 0);
            textView.setLayoutParams(params);
            textView.setId(i);
            textView.setOnClickListener(this);
            mTagsContainer.addView(textView);
        }

    }



    void parseJSON(JSONObject jsonObject) {
        try {
            JSONObject postObject = jsonObject.getJSONObject(Template.Global.ARRAY);
            RelativeLayout.LayoutParams relativeLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            mHeader.setLayoutParams(relativeLayoutParams);
            Picasso.with(getContext()).load(postObject.getString(Template.Detail.THUMBNAIL_LARGE)).into(new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    mDetailImage.setImageBitmap(bitmap);

                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {

                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
                    Picasso.with(getContext()).load(mImage).into(mDetailImage);
                }
            });
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            mHeaderContainer.setLayoutParams(layoutParams);

            //mDetailImage.setAdjustViewBounds(true);
            //RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            //mDetailImage.setLayoutParams(layoutParams);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(postObject.getString(Template.Detail.TITLE));
            mTitle.setText(postObject.getString(Template.Post.TITLE));
            mTag = postObject.getJSONArray("category").getJSONObject(0).getString("slug");
            mKategori.setText(Html.fromHtml(postObject.getJSONArray("category").getJSONObject(0).getString("name")));
            mKategori.setOnClickListener(this);
            mTagsArray = postObject.getJSONArray("tags");
            tagsTextView(mTagsArray.length());
            //mDate.setText(mTimeRelative.getRelativeTime(postObject.getString(Template.Post.DATE), getContext()));
            mDate.setText(postObject.getString(Template.Detail.DATE_CLEAR));
            Spanned spannedHtml = Html.fromHtml(postObject.getString(Template.Detail.CONTENT), this, null);
            mContent.setText(spannedHtml);


        } catch (JSONException e) {
            e.printStackTrace();

        }
    }


    @Override
    public Drawable getDrawable(String s) {
        LevelListDrawable d = new LevelListDrawable();
        d.setBounds(0, 0, mContentLayout.getHeight(), mContentLayout.getHeight());

        new LoadImage().execute(s, d);


        return d;
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        Intent intent = new Intent(getActivity(), TagsActivity.class);
        if (i == R.id.detailKategori) {
            intent.putExtra("slug", mTag);
            intent.putExtra("type", "kategori");
        } else {
            try {
                intent.putExtra("slug", mTagsArray.getJSONObject(i).getString("slug"));
                intent.putExtra("type", "tags");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        startActivity(intent);
        getActivity().finish();
    }


    class LoadImage extends AsyncTask<Object, Void, Bitmap> {

        private LevelListDrawable mDrawable;

        @Override
        protected Bitmap doInBackground(Object... params) {
            String source = (String) params[0];
            mDrawable = (LevelListDrawable) params[1];
            try {
                InputStream is = new URL(source).openStream();
                return BitmapFactory.decodeStream(is);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if (bitmap != null) {
                BitmapDrawable d = new BitmapDrawable(bitmap);
                mDrawable.addLevel(1, 1, d);
                int scale = d.getMinimumWidth()/d.getMinimumHeight();
                mDrawable.setBounds(0, 0, mContentLayout.getWidth() - 16, (mContentLayout.getWidth() / scale) - 16);
                mDrawable.setLevel(1);
                CharSequence t = mContent.getText();
                mContent.setText(t);
            }
        }
    }

    @Override
    public void onDestroy() {
        updateActionBarTransparency(1);
        updateStatusBarColor(1);
        super.onDestroy();
    }
}

