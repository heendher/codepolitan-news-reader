package com.heendher.codepolitannewsreadermobileapp.Fragment;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.heendher.codepolitannewsreadermobileapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ContactFragment extends Fragment implements View.OnClickListener {

    private ImageView mFacebook, mTwitter, mGoogle, mEmail;

    public ContactFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_contact, container, false);
        mFacebook = (ImageView) view.findViewById(R.id.contactFacebook);
        mFacebook.setOnClickListener(this);
        mTwitter = (ImageView)view.findViewById(R.id.contactTwitter);
        mTwitter.setOnClickListener(this);
        mGoogle = (ImageView)view.findViewById(R.id.contactGoogle);
        mGoogle.setOnClickListener(this);
        mEmail = (ImageView)view.findViewById(R.id.contactEmail);
        mEmail.setOnClickListener(this);
        return view;
    }

    void openURL(String url){
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(intent);
    }

    void openEmail(){
        Intent email = new Intent(Intent.ACTION_SEND);
        email.putExtra(Intent.EXTRA_EMAIL, new String[]{"contact@codepolitan.com"});
        email.setType("message/rfc822");
        startActivity(Intent.createChooser(email, "Choose an Email client :"));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.contactFacebook:
                openURL("https://www.facebook.com/codepolitan");
                break;
            case R.id.contactTwitter:
                openURL("https://twitter.com/codepolitan");
                break;
            case R.id.contactGoogle:
                openURL("http://plus.google.com/+codepolitan");
                break;
            case R.id.contactEmail:
                openEmail();
                break;
        }
    }
}
