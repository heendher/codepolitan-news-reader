package com.heendher.codepolitannewsreadermobileapp.Fragment;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.heendher.codepolitannewsreadermobileapp.R;
import com.squareup.picasso.Picasso;

/**
 * A simple {@link Fragment} subclass.
 */
public class OurPartnersFragment extends Fragment implements View.OnClickListener{

    private ImageView mChip, mKelas, mGeek, mFree, mKlik, mStartUp, mInfinys, mDicoding, mEdtech, mTech;

    public OurPartnersFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_our_partners, container, false);
        mChip = (ImageView)view.findViewById(R.id.imageChip);
        mKelas = (ImageView)view.findViewById(R.id.imageKelasKita);
        mGeek = (ImageView)view.findViewById(R.id.imageGeek);
        mFree = (ImageView)view.findViewById(R.id.imageFreenovation);
        mKlik = (ImageView)view.findViewById(R.id.imageKlik);
        mStartUp = (ImageView)view.findViewById(R.id.imageStartUp);
        mInfinys = (ImageView)view.findViewById(R.id.imageInfinysCloud);
        mDicoding = (ImageView)view.findViewById(R.id.imageDicoding);
        mEdtech = (ImageView)view.findViewById(R.id.imageEdtech);
        mTech = (ImageView)view.findViewById(R.id.imageTechna);
        Picasso.with(getContext()).load("http://www.codepolitan.com/wp-content/uploads/2015/08/cp_partner_startupbisnis.png").into(mStartUp);
        Picasso.with(getContext()).load("http://www.codepolitan.com/wp-content/uploads/2015/08/cp_partner_infinyscloud.jpg").into(mInfinys);
        Picasso.with(getContext()).load("http://www.codepolitan.com/wp-content/uploads/2015/08/cp_partner_chip.png").into(mChip);
        Picasso.with(getContext()).load("http://www.codepolitan.com/wp-content/uploads/2015/08/cp_partner_dicoding.png").into(mDicoding);
        Picasso.with(getContext()).load("http://www.codepolitan.com/wp-content/uploads/2015/08/cp_partner_klikindonesia.png").into(mKlik);
        Picasso.with(getContext()).load("http://www.codepolitan.com/wp-content/uploads/2015/08/cp_partner_edtech.jpg").into(mEdtech);
        Picasso.with(getContext()).load("http://www.codepolitan.com/wp-content/uploads/2015/08/cp_partner_freenovation.jpg").into(mFree);
        Picasso.with(getContext()).load("http://www.codepolitan.com/wp-content/uploads/2015/08/cp_partner_geekhunter.jpg").into(mGeek);
        Picasso.with(getContext()).load("http://www.codepolitan.com/wp-content/uploads/2015/08/cp_partner_tia.png").into(mTech);
        Picasso.with(getContext()).load("http://www.codepolitan.com/wp-content/uploads/2015/08/cp_partner_kelaskita.jpg").into(mKelas);
        mChip.setOnClickListener(this);
        mKelas.setOnClickListener(this);
        mGeek.setOnClickListener(this);
        mFree.setOnClickListener(this);
        mKlik.setOnClickListener(this);
        mStartUp.setOnClickListener(this);
        mInfinys.setOnClickListener(this);
        mDicoding.setOnClickListener(this);
        mEdtech.setOnClickListener(this);
        mTech.setOnClickListener(this);
        return view;
    }


    void openURL(String url){
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(intent);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imageChip:
                openURL("http://chip.co.id/");
                break;
            case R.id.imageKelasKita:
                openURL("https://kelaskita.com/");
                break;
            case R.id.imageGeek:
                openURL("http://geekhunter.co/");
                break;
            case R.id.imageFreenovation:
                openURL("http://freenovation.co/");
                break;
            case R.id.imageKlik:
                openURL("http://www.klikindonesia.org/");
                break;
            case R.id.imageStartUp:
                openURL("http://startupbisnis.com/");
                break;
            case R.id.imageInfinysCloud:
                openURL("http://infinyscloud.com/");
                break;
            case R.id.imageDicoding:
                openURL("http://dicoding.com/");
                break;
            case R.id.imageEdtech:
                openURL("http://edtechindo.org/");
                break;
            case R.id.imageTechna:
                openURL("https://id.techinasia.com/");
                break;
        }
    }
}
