package com.heendher.codepolitannewsreadermobileapp.POJO;

/**
 * Created by -H- on 8/30/2015.
 */
public class DrawerModel {
    int id;
    int idSelected;

    public DrawerModel(int id, int idSelected, String title) {
        this.id = id;
        this.idSelected = idSelected;
        this.title = title;
        this.isItem = true;
    }

    public int getIdSelected() {

        return idSelected;
    }

    public void setIdSelected(int idSelected) {
        this.idSelected = idSelected;
    }

    String title;
    boolean isItem = false;
    boolean isTitle = false;
    boolean isDivider = false;
    boolean isHeader = false;

    public boolean isHeader() {

        return isHeader;
    }

    public boolean isItem() {
        return isItem;
    }

    public boolean isTitle() {
        return isTitle;
    }

    public boolean isDivider() {
        return isDivider;
    }

    public int getId() {
        return id;

    }

    public DrawerModel(int id, String title){
        this.id = id;
        this.title = title;
        this.isItem = true;

    }

    public DrawerModel(String title){
        this.title = title;
        this.isTitle = true;
    }

    public DrawerModel(boolean isDivider){
        this.isDivider = true;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
