package com.heendher.codepolitannewsreadermobileapp.POJO;

/**
 * Created by -H- on 9/10/2015.
 */
public class SpinnerModel {
    String title;

    public SpinnerModel(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
}
