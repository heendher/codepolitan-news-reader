package com.heendher.codepolitannewsreadermobileapp.POJO;

/**
 * Created by -H- on 9/13/2015.
 */
public class TagsModel {
    private String mName;
    private String mSlug;
    private String mCount;

    public String getmCount() {
        return mCount;
    }

    public void setmCount(String mCount) {
        this.mCount = mCount;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getmSlug() {
        return mSlug;
    }

    public void setmSlug(String mSlug) {
        this.mSlug = mSlug;
    }
}


